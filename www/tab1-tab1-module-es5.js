(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["tab1-tab1-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/tab1/tab1.page.html":
/*!*********************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/tab1/tab1.page.html ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"end\">\n      <ion-menu-button>\n      </ion-menu-button>\n    </ion-buttons>\n    <ion-title>\n      CURRENCY CONVERTER\n    </ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <ion-refresher slot=\"fixed\" pullFactor=\"0.5\" pullMin=\"100\" pullMax=\"200\"\n    (ionRefresh)=\"refreshExRateFromCache($event)\">\n    <ion-refresher-content pullingIcon=\"arrow-dropdown\">\n    </ion-refresher-content>\n  </ion-refresher>\n  <ion-grid>\n    <ion-row>\n      <ion-col>\n        <ion-card class=\"welcome-card\">\n          <ion-card-content>\n            <ion-grid>\n              <ion-row>\n                <ion-col>\n                  <ion-card>\n                    <ion-card-content>\n                      <ion-row>\n                        <ion-col class=\"ion-text-center\">\n                          <h1>\n                            Base Currency: {{ base }}\n                          </h1>\n                        </ion-col>\n                        <ion-col>\n                          <ion-thumbnail>\n                            <img src=\"../../../assets/imgs/countries/{{base}}.png\" />\n                          </ion-thumbnail>\n                        </ion-col>\n                      </ion-row>\n                      <ion-row class=\"dropDownList\">\n                        <ion-col>\n                          <ion-button fill=\"outline\" (click)=\"toogleDropDown()\">\n                            <ion-icon slot=\"icon-only\" name=\"arrow-dropdown\"></ion-icon>\n                          </ion-button>\n                          <ion-list *ngIf=\"showList\" class=\"data-container\" (blur)=\"toogleDropDown()\">\n                            <ion-searchbar [(ngModel)]=\"searchS\" autocomplete=\"on\" animated\n                              (ionChange)=\"searchSelect()\"></ion-searchbar>\n                            <ion-item *ngFor=\"let currency of currenciesSelect\"\n                              (click)=\"updateBase(currency.id, $event)\">\n                              <ion-label slot=\"start\">{{currency.id}} </ion-label>\n                              <ion-thumbnail><img src=\"{{currency.img}}\" /></ion-thumbnail>\n                            </ion-item>\n                          </ion-list>\n                        </ion-col>\n                        <ion-col>\n                          <ion-label>Change Base Currency</ion-label>\n                        </ion-col>\n                      </ion-row>\n                    </ion-card-content>\n                  </ion-card>\n                </ion-col>\n              </ion-row>\n              <ion-row>\n                <ion-col class=\"ion-text-center\">\n                  <ion-input min=\"0\" inputmode=\"numeric\" [(ngModel)]=\"quantity\" type=\"number\" placeholder=\"Quantity...\"\n                    (ionChange)=\"calculate()\">\n                  </ion-input>\n                </ion-col>\n              </ion-row>\n              <ion-row>\n                <ion-col class=\"ion-text-center\">\n                  <ion-icon name=\"swap\"></ion-icon>\n                </ion-col>\n              </ion-row>\n            </ion-grid>\n          </ion-card-content>\n        </ion-card>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col>\n        <ion-searchbar [(ngModel)]=\"searcher\" autocomplete=\"on\" animated (ionChange)=\"search()\"></ion-searchbar>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n  <ion-list>\n    <ion-list-header>\n      <ion-label>Exchanges: </ion-label>\n    </ion-list-header>\n    <ion-item *ngFor=\"let rate of filteredCurrencies\">\n      <ion-grid>\n        <ion-row>\n          <ion-col>\n            <ion-thumbnail>\n              <img [src]=\"rate.img\" />\n            </ion-thumbnail>\n          </ion-col>\n          <ion-col>\n            {{ rate.id }}\n          </ion-col>\n          <ion-col>\n            {{ rate.value | currency: rate.id :true}}\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n    </ion-item>\n  </ion-list>\n</ion-content>"

/***/ }),

/***/ "./src/app/pages/tab1/tab1.module.ts":
/*!*******************************************!*\
  !*** ./src/app/pages/tab1/tab1.module.ts ***!
  \*******************************************/
/*! exports provided: Tab1PageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Tab1PageModule", function() { return Tab1PageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _tab1_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./tab1.page */ "./src/app/pages/tab1/tab1.page.ts");







var Tab1PageModule = /** @class */ (function () {
    function Tab1PageModule() {
    }
    Tab1PageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["NgModule"])({
            imports: [
                _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonicModule"],
                _angular_common__WEBPACK_IMPORTED_MODULE_4__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild([{ path: '', component: _tab1_page__WEBPACK_IMPORTED_MODULE_6__["Tab1Page"] }])
            ],
            declarations: [_tab1_page__WEBPACK_IMPORTED_MODULE_6__["Tab1Page"]]
        })
    ], Tab1PageModule);
    return Tab1PageModule;
}());



/***/ }),

/***/ "./src/app/pages/tab1/tab1.page.scss":
/*!*******************************************!*\
  !*** ./src/app/pages/tab1/tab1.page.scss ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".welcome-card img {\n  max-height: 35vh;\n  overflow: hidden;\n}\n\n.dropDownList {\n  border: 1px solid #222020;\n  width: 100%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy95dWthL0RvY3VtZW50cy9wcm9qZWN0cy9DdXJyZW5jeUV4Y2hhbmdlci9zcmMvYXBwL3BhZ2VzL3RhYjEvdGFiMS5wYWdlLnNjc3MiLCJzcmMvYXBwL3BhZ2VzL3RhYjEvdGFiMS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxnQkFBQTtFQUNBLGdCQUFBO0FDQ0Y7O0FERUE7RUFDRSx5QkFBQTtFQUNBLFdBQUE7QUNDRiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3RhYjEvdGFiMS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIud2VsY29tZS1jYXJkIGltZyB7XG4gIG1heC1oZWlnaHQ6IDM1dmg7XG4gIG92ZXJmbG93OiBoaWRkZW47XG59XG4gXG4uZHJvcERvd25MaXN0IHtcbiAgYm9yZGVyOiAxcHggc29saWQgcmdiKDM0LCAzMiwgMzIpO1xuICB3aWR0aDogMTAwJTtcbn1cbiAgIiwiLndlbGNvbWUtY2FyZCBpbWcge1xuICBtYXgtaGVpZ2h0OiAzNXZoO1xuICBvdmVyZmxvdzogaGlkZGVuO1xufVxuXG4uZHJvcERvd25MaXN0IHtcbiAgYm9yZGVyOiAxcHggc29saWQgIzIyMjAyMDtcbiAgd2lkdGg6IDEwMCU7XG59Il19 */"

/***/ }),

/***/ "./src/app/pages/tab1/tab1.page.ts":
/*!*****************************************!*\
  !*** ./src/app/pages/tab1/tab1.page.ts ***!
  \*****************************************/
/*! exports provided: Tab1Page */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Tab1Page", function() { return Tab1Page; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ionic_cache_observable__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ionic-cache-observable */ "./node_modules/ionic-cache-observable/index.js");
/* harmony import */ var ionic_cache_observable__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(ionic_cache_observable__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var src_app_providers_exchanges_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/providers/exchanges.service */ "./src/app/providers/exchanges.service.ts");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm5/ionic-storage.js");






var Tab1Page = /** @class */ (function () {
    function Tab1Page(platform, cacheService, exchangProvider, storage) {
        var _this = this;
        this.platform = platform;
        this.cacheService = cacheService;
        this.exchangProvider = exchangProvider;
        this.storage = storage;
        this.base = '';
        this.filteredCurrencies = [];
        this.allCurrenciesRates = [];
        this.currenciesSelect = [];
        this.values = [];
        this.flags = [];
        this.quantity = 1;
        this.showList = false;
        this.platform.ready().then(function () {
            _this.storage.get('last').then(function (last) {
                if (last) {
                    _this.base = last;
                }
                else {
                    _this.base = 'USD';
                }
            });
        });
    }
    Tab1Page.prototype.compareWith = function (o1, o2) {
        return o1 && o2 ? o1.id === o2.id : o1 === o2;
    };
    ;
    Tab1Page.prototype.ionViewWillEnter = function () {
        this.registerInCache(this.base);
    };
    Tab1Page.prototype.toogleDropDown = function () {
        this.showList = !this.showList;
    };
    Tab1Page.prototype.getCurrencyList = function (jsonObject) {
        this.filteredCurrencies = [];
        this.allCurrenciesRates = [];
        this.date = jsonObject.date;
        this.base = jsonObject.base;
        for (var element in jsonObject.rates) {
            this.allCurrenciesRates.push({
                id: element,
                img: '../../../assets/imgs/flags/' + element + '.png',
                value: jsonObject.rates[element]
            });
            this.flags.push('../../../assets/imgs/flags/' + element + '.png');
            this.values[element] = jsonObject.rates[element];
            this.filteredCurrencies = Object.assign([], this.allCurrenciesRates);
            this.currenciesSelect = Object.assign([], this.allCurrenciesRates);
        }
    };
    Tab1Page.prototype.search = function () {
        var _this = this;
        if (this.searcher !== '' && this.searcher !== null && this.searcher !== undefined) {
            this.filteredCurrencies = this.allCurrenciesRates.filter(function (currency) {
                return currency.id.toLowerCase().indexOf(_this.searcher.toLowerCase()) > -1;
            });
        }
        else {
            this.filteredCurrencies = Object.assign([], this.allCurrenciesRates);
        }
        this.setValues();
    };
    Tab1Page.prototype.setValues = function () {
        for (var i = 0; i < this.filteredCurrencies.length; i++) {
            this.filteredCurrencies[i].value = this.values[this.filteredCurrencies[i].id] * this.quantity;
        }
    };
    Tab1Page.prototype.calculate = function () {
        if (this.quantity !== null) {
            for (var i = 0; i < this.filteredCurrencies.length; i++) {
                var field = this.values[this.filteredCurrencies[i].id];
                this.filteredCurrencies[i].value = field * this.quantity;
            }
        }
    };
    Tab1Page.prototype.refreshExRateFromCache = function (event) {
        var _this = this;
        this.cacheService
            .get(this.base)
            .mergeMap(function (cache) { return cache.refresh(); })
            .subscribe(function (exRate) {
            _this.exRate = JSON.parse(JSON.stringify(exRate));
            _this.getCurrencyList(_this.exRate);
        });
        setTimeout(function () {
            event.target.complete();
        }, 2000);
    };
    Tab1Page.prototype.updateBase = function (base, event) {
        this.showList = false;
        this.base = base;
        this.storage.set('last', base);
        this.registerInCache(base);
        setTimeout(function () {
            event.target.complete();
        }, 2000);
    };
    Tab1Page.prototype.registerInCache = function (base) {
        var _this = this;
        var dataObservable = this.exchangProvider.getExchange(base);
        this.cacheSubscription = this.cacheService.register('exRate', dataObservable)
            .mergeMap(function (cache) {
            _this.cache = cache;
            _this.cache.refresh().subscribe();
            return _this.cache.get$;
        }).subscribe(function (exRate) {
            _this.exRate = JSON.parse(JSON.stringify(exRate));
            _this.getCurrencyList(_this.exRate);
        });
    };
    Tab1Page.prototype.searchSelect = function () {
        var _this = this;
        this.showList = true;
        if (this.searchS !== '' && this.searchS !== null && this.searchS !== undefined) {
            this.currenciesSelect = this.allCurrenciesRates.filter(function (currency) {
                return currency.id.toLowerCase().indexOf(_this.searchS.toLowerCase()) > -1;
            });
        }
        else {
            this.currenciesSelect = Object.assign([], this.allCurrenciesRates);
        }
    };
    Tab1Page.ctorParameters = function () { return [
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["Platform"] },
        { type: ionic_cache_observable__WEBPACK_IMPORTED_MODULE_3__["CacheService"] },
        { type: src_app_providers_exchanges_service__WEBPACK_IMPORTED_MODULE_4__["ExchangesService"] },
        { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"] }
    ]; };
    Tab1Page = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: 'app-tab1',
            template: __webpack_require__(/*! raw-loader!./tab1.page.html */ "./node_modules/raw-loader/index.js!./src/app/pages/tab1/tab1.page.html"),
            styles: [__webpack_require__(/*! ./tab1.page.scss */ "./src/app/pages/tab1/tab1.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_1__["Platform"],
            ionic_cache_observable__WEBPACK_IMPORTED_MODULE_3__["CacheService"],
            src_app_providers_exchanges_service__WEBPACK_IMPORTED_MODULE_4__["ExchangesService"],
            _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"]])
    ], Tab1Page);
    return Tab1Page;
}());



/***/ })

}]);
//# sourceMappingURL=tab1-tab1-module-es5.js.map