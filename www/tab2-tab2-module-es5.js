(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["tab2-tab2-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/tab2/tab2.page.html":
/*!*********************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/tab2/tab2.page.html ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar>\n      <ion-buttons slot=\"end\">\n          <ion-menu-button>\n          </ion-menu-button>\n        </ion-buttons>\n    <ion-title>\n      EXCHANGE COMPARISON HISTORY\n    </ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content class=\"ion-padding\">\n  <ion-refresher slot=\"fixed\" pullFactor=\"0.5\" pullMin=\"100\" pullMax=\"200\"\n    (ionRefresh)=\"refreshExRateFromCache($event)\">\n    <ion-refresher-content pullingIcon=\"arrow-dropdown\">\n    </ion-refresher-content>\n  </ion-refresher>\n  <ion-grid>\n    <ion-row>\n      <ion-col class=\"col-md-6\">\n        <ion-card>\n          <ion-card-header>\n            <ion-card-title>\n              <ion-grid>\n                <ion-row>\n                  <ion-col>\n                    Look Up:\n                  </ion-col>\n                </ion-row>\n                <ion-row>\n                  <ion-col>\n                    <h2> FROM: {{from | date}}</h2>\n                  </ion-col>\n                  <ion-col>\n                      <h2> TO: {{to | date}}</h2>\n                    </ion-col>\n                </ion-row>\n                <ion-row>\n                  <ion-col>\n                    <ion-item>\n                      <ion-label>yyyy-mm-dd</ion-label>\n                      <ion-datetime displayFormat=\"YYYY-MM-DD\"  (ionChange)=\"updateDate(base, $event)\" placeholder=\"Select FROM Date\" [(ngModel)]=\"date1\"></ion-datetime>\n                    </ion-item>\n                  </ion-col>\n                  <ion-col>\n                      <ion-item>\n                        <ion-label>yyyy-mm-dd</ion-label>\n                        <ion-datetime displayFormat=\"YYYY-MM-DD\" (ionChange)=\"updateDate(base, $event)\" placeholder=\"Select TO Date\" [(ngModel)]=\"date2\"></ion-datetime>\n                      </ion-item>\n                    </ion-col>\n                </ion-row>\n              </ion-grid>\n            </ion-card-title>\n          </ion-card-header>\n          <ion-card-content>\n            <ion-grid>\n              <ion-row>\n                <ion-col>\n                  <h1>\n                    {{baseCurrency}}\n                  </h1>\n                  <ion-thumbnail>\n                    <img src=\"../../../assets/imgs/countries/{{baseCurrency}}\" />\n                  </ion-thumbnail>\n                </ion-col>\n                <ion-col>\n                  <ion-icon name=\"swap\"></ion-icon>\n                </ion-col>\n                <ion-col>\n                  <h1>\n                    {{compareTo}}\n                  </h1>\n                  <ion-thumbnail>\n                    <img src=\"../../../assets/imgs/countries/{{compareTo}}\" />\n                  </ion-thumbnail>\n                </ion-col>\n              </ion-row>\n              <ion-row>\n                <ion-col class=\"drop-down\">\n                  <ion-button fill=\"outline\" (click)=\"toogleDropDown()\">\n                    <ion-icon slot=\"icon-only\" name=\"arrow-dropdown\"></ion-icon>\n                  </ion-button>\n                  <ion-list *ngIf=\"showDropDown\" class=\"data-container\" (blur)=\"toogleDropDown()\">\n                    <ion-searchbar [(ngModel)]=\"searchS\" autocomplete=\"on\" animated (ionChange)=\"searchSelect()\">\n                    </ion-searchbar>\n                    <ion-item *ngFor=\"let currency of currenciesSelect\" (click)=\"updateBase(currency.id, $event)\">\n                      <ion-label slot=\"start\">{{currency.id}} </ion-label>\n                      <ion-thumbnail><img src=\"{{currency.img}}\" /></ion-thumbnail>\n                    </ion-item>\n                  </ion-list>\n                </ion-col>\n                <ion-col>\n                </ion-col>\n                <ion-col class=\"drop-down\">\n                  <ion-button fill=\"outline\" (click)=\"toogleDropDown2()\">\n                    <ion-icon slot=\"icon-only\" name=\"arrow-dropdown\"></ion-icon>\n                  </ion-button>\n                  <ion-list *ngIf=\"showDropDownTo\" class=\"data-container\" (blur)=\"toogleDropDown2()\">\n                    <ion-searchbar [(ngModel)]=\"searchS2\" autocomplete=\"on\" animated\n                      (ionChange)=\"searchSelectCompareTo()\"></ion-searchbar>\n                    <ion-item *ngFor=\"let currency of currenciesSelect2\" (click)=\"updateTo(currency.id, $event)\">\n                      <ion-label slot=\"start\">{{currency.id}} </ion-label>\n                      <ion-thumbnail><img src=\"{{currency.img}}\" /></ion-thumbnail>\n                    </ion-item>\n                  </ion-list>\n                </ion-col>\n              </ion-row>\n            </ion-grid>\n          </ion-card-content>\n        </ion-card>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col class=\"col-md-6\">\n        <div style=\"display: block;\">\n          <canvas baseChart width=\"300\" height=\"400\" [datasets]=\"lineChartData\" [labels]=\"lineChartLabels\"\n            [options]=\"lineChartOptions\" [colors]=\"lineChartColors\" [legend]=\"lineChartLegend\"\n            [chartType]=\"lineChartType\" (chartHover)=\"chartHovered($event)\"\n            (chartClick)=\"chartClicked($event)\"></canvas>\n        </div>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-content>"

/***/ }),

/***/ "./src/app/pages/tab2/tab2.module.ts":
/*!*******************************************!*\
  !*** ./src/app/pages/tab2/tab2.module.ts ***!
  \*******************************************/
/*! exports provided: Tab2PageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Tab2PageModule", function() { return Tab2PageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _tab2_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./tab2.page */ "./src/app/pages/tab2/tab2.page.ts");
/* harmony import */ var ng2_charts__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ng2-charts */ "./node_modules/ng2-charts/fesm5/ng2-charts.js");








var Tab2PageModule = /** @class */ (function () {
    function Tab2PageModule() {
    }
    Tab2PageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["NgModule"])({
            imports: [
                _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonicModule"],
                _angular_common__WEBPACK_IMPORTED_MODULE_4__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild([{ path: '', component: _tab2_page__WEBPACK_IMPORTED_MODULE_6__["Tab2Page"] }]),
                ng2_charts__WEBPACK_IMPORTED_MODULE_7__["ChartsModule"],
            ],
            declarations: [_tab2_page__WEBPACK_IMPORTED_MODULE_6__["Tab2Page"]]
        })
    ], Tab2PageModule);
    return Tab2PageModule;
}());



/***/ }),

/***/ "./src/app/pages/tab2/tab2.page.scss":
/*!*******************************************!*\
  !*** ./src/app/pages/tab2/tab2.page.scss ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".combo-container {\n  position: fixed;\n  z-index: 9;\n  background: white;\n}\n\n.input-control {\n  width: 165px;\n  border: 0;\n}\n\n.data-container {\n  width: 196px;\n  background: white;\n  display: block;\n  position: relative;\n  border: 1px solid grey;\n  height: 200px;\n  overflow-y: scroll;\n  top: -1px;\n}\n\n.data-list {\n  cursor: pointer;\n  margin-top: 2px;\n  margin-bottom: 2px;\n}\n\n.drop-down {\n  border: 1px solid grey;\n  width: 100%;\n}\n\n.drop-down-button {\n  border: 0;\n  background: white;\n}\n\n.highlight {\n  background: grey;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy95dWthL0RvY3VtZW50cy9wcm9qZWN0cy9DdXJyZW5jeUV4Y2hhbmdlci9zcmMvYXBwL3BhZ2VzL3RhYjIvdGFiMi5wYWdlLnNjc3MiLCJzcmMvYXBwL3BhZ2VzL3RhYjIvdGFiMi5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0E7RUFDSSxlQUFBO0VBQ0EsVUFBQTtFQUNBLGlCQUFBO0FDQUo7O0FERUk7RUFDQSxZQUFBO0VBQ0EsU0FBQTtBQ0NKOztBRENJO0VBQ0EsWUFBQTtFQUNBLGlCQUFBO0VBQ0EsY0FBQTtFQUNBLGtCQUFBO0VBQ0Esc0JBQUE7RUFDQSxhQUFBO0VBQ0Esa0JBQUE7RUFDQSxTQUFBO0FDRUo7O0FEQUk7RUFDQSxlQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0FDR0o7O0FEREk7RUFDQSxzQkFBQTtFQUNBLFdBQUE7QUNJSjs7QURGSTtFQUNBLFNBQUE7RUFDQSxpQkFBQTtBQ0tKOztBREhJO0VBQ0EsZ0JBQUE7QUNNSiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3RhYjIvdGFiMi5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJcbi5jb21iby1jb250YWluZXIge1xuICAgIHBvc2l0aW9uOiBmaXhlZDtcbiAgICB6LWluZGV4OiA5O1xuICAgIGJhY2tncm91bmQ6IHdoaXRlO1xuICAgIH1cbiAgICAuaW5wdXQtY29udHJvbHtcbiAgICB3aWR0aDogMTY1cHg7XG4gICAgYm9yZGVyOiAwO1xuICAgIH1cbiAgICAuZGF0YS1jb250YWluZXIge1xuICAgIHdpZHRoOiAxOTZweDtcbiAgICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgICBkaXNwbGF5OiBibG9jaztcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgYm9yZGVyOiAxcHggc29saWQgZ3JleTtcbiAgICBoZWlnaHQ6IDIwMHB4O1xuICAgIG92ZXJmbG93LXk6IHNjcm9sbDtcbiAgICB0b3A6IC0xcHg7XG4gICAgfVxuICAgIC5kYXRhLWxpc3Qge1xuICAgIGN1cnNvcjogcG9pbnRlcjtcbiAgICBtYXJnaW4tdG9wOiAycHg7XG4gICAgbWFyZ2luLWJvdHRvbTogMnB4O1xuICAgIH1cbiAgICAuZHJvcC1kb3duIHtcbiAgICBib3JkZXI6IDFweCBzb2xpZCBncmV5O1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIH1cbiAgICAuZHJvcC1kb3duLWJ1dHRvbiB7XG4gICAgYm9yZGVyOiAwO1xuICAgIGJhY2tncm91bmQ6IHdoaXRlO1xuICAgIH1cbiAgICAuaGlnaGxpZ2h0e1xuICAgIGJhY2tncm91bmQ6IGdyZXk7XG4gICAgfSIsIi5jb21iby1jb250YWluZXIge1xuICBwb3NpdGlvbjogZml4ZWQ7XG4gIHotaW5kZXg6IDk7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xufVxuXG4uaW5wdXQtY29udHJvbCB7XG4gIHdpZHRoOiAxNjVweDtcbiAgYm9yZGVyOiAwO1xufVxuXG4uZGF0YS1jb250YWluZXIge1xuICB3aWR0aDogMTk2cHg7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xuICBkaXNwbGF5OiBibG9jaztcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBib3JkZXI6IDFweCBzb2xpZCBncmV5O1xuICBoZWlnaHQ6IDIwMHB4O1xuICBvdmVyZmxvdy15OiBzY3JvbGw7XG4gIHRvcDogLTFweDtcbn1cblxuLmRhdGEtbGlzdCB7XG4gIGN1cnNvcjogcG9pbnRlcjtcbiAgbWFyZ2luLXRvcDogMnB4O1xuICBtYXJnaW4tYm90dG9tOiAycHg7XG59XG5cbi5kcm9wLWRvd24ge1xuICBib3JkZXI6IDFweCBzb2xpZCBncmV5O1xuICB3aWR0aDogMTAwJTtcbn1cblxuLmRyb3AtZG93bi1idXR0b24ge1xuICBib3JkZXI6IDA7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xufVxuXG4uaGlnaGxpZ2h0IHtcbiAgYmFja2dyb3VuZDogZ3JleTtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/pages/tab2/tab2.page.ts":
/*!*****************************************!*\
  !*** ./src/app/pages/tab2/tab2.page.ts ***!
  \*****************************************/
/*! exports provided: Tab2Page */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Tab2Page", function() { return Tab2Page; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ionic_cache_observable__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ionic-cache-observable */ "./node_modules/ionic-cache-observable/index.js");
/* harmony import */ var ionic_cache_observable__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(ionic_cache_observable__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var src_app_providers_exchanges_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/providers/exchanges.service */ "./src/app/providers/exchanges.service.ts");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm5/ionic-storage.js");
/* harmony import */ var src_app_providers_dates_pipe_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/providers/dates-pipe.service */ "./src/app/providers/dates-pipe.service.ts");







var Tab2Page = /** @class */ (function () {
    function Tab2Page(platform, cacheService, exchangProvider, storage, datePipe) {
        var _this = this;
        this.platform = platform;
        this.cacheService = cacheService;
        this.exchangProvider = exchangProvider;
        this.storage = storage;
        this.datePipe = datePipe;
        this.baseCurrency = '';
        this.compareTo = '';
        this.filteredCurrencies = [];
        this.allCurrenciesRates = [];
        this.currencyList = [];
        this.currenciesSelect = [];
        this.currenciesSelect2 = [];
        this.values = [];
        this.flags = [];
        this.quantity = 1;
        this.showDropDown = false;
        this.showDropDownTo = false;
        this.from = '2019-01-01';
        this.lineChartLegend = true;
        this.lineChartType = 'line';
        this.lineChartData = [
            { data: [65, 59, 80, 81, 56, 55, 40], label: 'Series A' },
            { data: [28, 48, 40, 19, 86, 27, 90], label: 'Series B' },
        ];
        this.lineChartLabels = ['January', 'February', 'March', 'April', 'May', 'June', 'July'];
        this.lineChartOptions = {
            responsive: true
        };
        this.lineChartColors = [
            {
                backgroundColor: 'rgba(148,159,177,0.2)',
                borderColor: 'rgba(148,159,177,1)',
                pointBackgroundColor: 'rgba(148,159,177,1)',
                pointBorderColor: '#fff',
                pointHoverBackgroundColor: '#fff',
                pointHoverBorderColor: 'rgba(148,159,177,0.8)'
            },
            {
                backgroundColor: 'rgba(77,83,96,0.2)',
                borderColor: 'rgba(77,83,96,1)',
                pointBackgroundColor: 'rgba(77,83,96,1)',
                pointBorderColor: '#fff',
                pointHoverBackgroundColor: '#fff',
                pointHoverBorderColor: 'rgba(77,83,96,1)'
            },
            {
                backgroundColor: 'rgba(148,159,177,0.2)',
                borderColor: 'rgba(148,159,177,1)',
                pointBackgroundColor: 'rgba(148,159,177,1)',
                pointBorderColor: '#fff',
                pointHoverBackgroundColor: '#fff',
                pointHoverBorderColor: 'rgba(148,159,177,0.8)'
            }
        ];
        this.platform.ready().then(function () {
            _this.getLastBase();
            _this.getCompareTo();
            _this.getFromTime();
            _this.getToTime();
            _this.currencyList = _this.exchangProvider.getCurrencyList();
        });
    }
    Tab2Page.prototype.ionViewWillEnter = function () {
        this.registerInCache(this.baseCurrency);
    };
    Tab2Page.prototype.seletFromTime = function () {
    };
    Tab2Page.prototype.getLastBase = function () {
        var _this = this;
        this.storage.get('lasth').then(function (last) {
            if (last) {
                _this.baseCurrency = last;
            }
            else {
                _this.baseCurrency = 'USD';
            }
        });
    };
    Tab2Page.prototype.getCompareTo = function () {
        var _this = this;
        this.storage.get('compareTo').then(function (c) {
            if (c) {
                _this.compareTo = c;
            }
            else {
                _this.compareTo = 'EUR';
            }
        });
    };
    Tab2Page.prototype.getFromTime = function () {
        var _this = this;
        this.storage.get('from').then(function (f) {
            if (f) {
                _this.from = f;
            }
        });
    };
    Tab2Page.prototype.getToTime = function () {
        var _this = this;
        this.storage.get('to').then(function (to) {
            if (to) {
                _this.to = to;
            }
            else {
                _this.to = _this.datePipe.transformDate(new Date());
            }
        });
    };
    Tab2Page.prototype.getValues = function (jsonObject) {
        console.log('in getValues');
        this.lineChartLabels = [];
        this.lineChartData[0].data = [];
        this.lineChartData[0].label = this.baseCurrency;
        this.lineChartData[1].data = [];
        this.lineChartData[1].label = this.compareTo;
        for (var element_1 in jsonObject.rates) {
            this.lineChartLabels.push(element_1);
            this.lineChartData[0].data.push(1);
            this.lineChartData[1].data.push(jsonObject.rates[element_1][this.compareTo]);
        }
    };
    Tab2Page.prototype.refreshExRateFromCache = function (event) {
        var _this = this;
        this.cacheService
            .get(this.baseCurrency)
            .mergeMap(function (cache) { return cache.refresh(); })
            .subscribe(function (exRate) {
            _this.exchangeHistory = JSON.parse(JSON.stringify(exRate));
            _this.getValues(_this.exchangeHistory);
            _this.getCurrencyList(_this.exchangeHistory);
        });
        setTimeout(function () {
            event.target.complete();
        }, 2000);
    };
    Tab2Page.prototype.updateDate = function (base, event) {
        if (this.date1 !== undefined && this.date1 !== null) {
            this.from = this.datePipe.transformDate(this.date1);
        }
        if (this.date2 !== undefined && this.date2 !== null) {
            this.to = this.datePipe.transformDate(this.date2);
        }
        this.updateBase(base, event);
    };
    Tab2Page.prototype.updateBase = function (base, event) {
        this.showDropDown = false;
        this.baseCurrency = base;
        this.storage.set('lasth', base);
        this.registerInCache(base);
        setTimeout(function () {
            event.target.complete();
        }, 2000);
    };
    Tab2Page.prototype.updateTo = function (compTo, event) {
        this.showDropDownTo = false;
        this.compareTo = compTo;
        this.storage.set('compareTo', compTo);
        this.getValues(this.exchangeHistory);
        setTimeout(function () {
            event.target.complete();
        }, 2000);
    };
    Tab2Page.prototype.registerInCache = function (base) {
        var _this = this;
        var dataObservable = this.exchangProvider.getHistoryFromBase(base, this.from, this.to);
        this.cacheSubscription = this.cacheService.register('exRateHistory', dataObservable)
            .mergeMap(function (cache) {
            _this.cache = cache;
            _this.cache.refresh().subscribe();
            return _this.cache.get$;
        }).subscribe(function (exRate) {
            _this.exchangeHistory = JSON.parse(JSON.stringify(exRate));
            _this.getValues(_this.exchangeHistory);
            _this.getCurrencyList(_this.exchangeHistory);
        });
    };
    Tab2Page.prototype.getCurrencyList = function (jsonObject) {
        console.log('in getCurrencyList ');
        this.filteredCurrencies = [];
        this.allCurrenciesRates = [];
        this.from = jsonObject.start_at;
        this.baseCurrency = jsonObject.base;
        this.to = jsonObject.end_at;
        for (var i = 0; i < this.currencyList.length; i++) {
            this.allCurrenciesRates.push({
                id: this.currencyList[i],
                img: '../../../assets/imgs/flags/' + this.currencyList[i] + '.png',
            });
            this.filteredCurrencies = Object.assign([], this.allCurrenciesRates);
            this.currenciesSelect = Object.assign([], this.allCurrenciesRates);
            this.currenciesSelect2 = Object.assign([], this.allCurrenciesRates);
        }
    };
    Tab2Page.prototype.toogleDropDown = function () {
        this.showDropDown = !this.showDropDown;
    };
    Tab2Page.prototype.toogleDropDown2 = function () {
        this.showDropDownTo = !this.showDropDownTo;
    };
    Tab2Page.prototype.searchSelect = function () {
        var _this = this;
        this.showDropDown = true;
        if (this.searchS !== '' && this.searchS !== null && this.searchS !== undefined) {
            this.currenciesSelect = this.allCurrenciesRates.filter(function (currency) {
                return currency.id.toLowerCase().indexOf(_this.searchS.toLowerCase()) > -1;
            });
        }
        else {
            this.currenciesSelect = Object.assign([], this.allCurrenciesRates);
        }
    };
    Tab2Page.prototype.searchSelectCompareTo = function () {
        var _this = this;
        this.showDropDown = true;
        if (this.searchS2 !== '' && this.searchS2 !== null && this.searchS2 !== undefined) {
            this.currenciesSelect2 = this.allCurrenciesRates.filter(function (currency) {
                return currency.id.toLowerCase().indexOf(_this.searchS2.toLowerCase()) > -1;
            });
        }
        else {
            this.currenciesSelect2 = Object.assign([], this.allCurrenciesRates);
        }
    };
    Tab2Page.ctorParameters = function () { return [
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["Platform"] },
        { type: ionic_cache_observable__WEBPACK_IMPORTED_MODULE_3__["CacheService"] },
        { type: src_app_providers_exchanges_service__WEBPACK_IMPORTED_MODULE_4__["ExchangesService"] },
        { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"] },
        { type: src_app_providers_dates_pipe_service__WEBPACK_IMPORTED_MODULE_6__["DatesPipeService"] }
    ]; };
    Tab2Page = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: 'app-tab2',
            template: __webpack_require__(/*! raw-loader!./tab2.page.html */ "./node_modules/raw-loader/index.js!./src/app/pages/tab2/tab2.page.html"),
            styles: [__webpack_require__(/*! ./tab2.page.scss */ "./src/app/pages/tab2/tab2.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_1__["Platform"],
            ionic_cache_observable__WEBPACK_IMPORTED_MODULE_3__["CacheService"],
            src_app_providers_exchanges_service__WEBPACK_IMPORTED_MODULE_4__["ExchangesService"],
            _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"],
            src_app_providers_dates_pipe_service__WEBPACK_IMPORTED_MODULE_6__["DatesPipeService"]])
    ], Tab2Page);
    return Tab2Page;
}());



/***/ }),

/***/ "./src/app/providers/dates-pipe.service.ts":
/*!*************************************************!*\
  !*** ./src/app/providers/dates-pipe.service.ts ***!
  \*************************************************/
/*! exports provided: DatesPipeService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DatesPipeService", function() { return DatesPipeService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");



var DatesPipeService = /** @class */ (function () {
    function DatesPipeService(datePipe) {
        this.datePipe = datePipe;
    }
    DatesPipeService.prototype.transformDate = function (date) {
        return this.datePipe.transform(date, 'yyyy-MM-dd').toString();
    };
    DatesPipeService.ctorParameters = function () { return [
        { type: _angular_common__WEBPACK_IMPORTED_MODULE_2__["DatePipe"] }
    ]; };
    DatesPipeService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common__WEBPACK_IMPORTED_MODULE_2__["DatePipe"]])
    ], DatesPipeService);
    return DatesPipeService;
}());



/***/ })

}]);
//# sourceMappingURL=tab2-tab2-module-es5.js.map