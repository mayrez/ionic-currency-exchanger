import { element } from 'protractor';
import { RateHistory } from './../../providers/exchanges.service';
import { Platform } from '@ionic/angular';
import { Component } from '@angular/core';
import { Observable, Subscription } from 'rxjs/';
import { CacheService, Cache } from 'ionic-cache-observable';
import { ExchangesService, RateLatest } from 'src/app/providers/exchanges.service';
import { Storage } from '@ionic/storage';
import { DatesPipeService } from 'src/app/providers/dates-pipe.service';
@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {
  public data$: Observable<RateHistory>;
  private cache: Cache<RateHistory>;
  private cacheSubscription: Subscription;
  exRate: RateHistory;
  base: string= '';
  compareTo: string = '';
  start_at: string;
  searcher:string;
  searchS: string;
  searchS2: string;
  date1: Date;
  date2: Date;
  filteredCurrencies= [];
  allCurrenciesRates = [];
  currencyList = [];
  currenciesSelect = [];
  currenciesSelect2 = [];
  values = [];
  flags = [];
  quantity: number = 1;
  showDropDown: boolean = false;
  showDropDownTo: boolean = false;
  from: string = '2019-01-01';
  to: string;
  public lineChartLegend:boolean = true;
  public lineChartType:string = 'line';

  constructor(
    private platform: Platform,
    private cacheService: CacheService,
    private exchangProvider: ExchangesService,
    private storage: Storage,
    private datePipe: DatesPipeService
  ) {
    this.platform.ready().then(()=>{
      this.getLastBase();
      this.getCompareTo();
      this.getFromTime();
      this.getToTime();
      this.currencyList = this.exchangProvider.getCurrencyList();
    });
  }
  ionViewWillEnter(){
   this.registerInCache(this.base);
  }
  seletFromTime(){
    
  }
  getLastBase(){
    this.storage.get('lasth').then((last)=>{
      if(last){
        this.base = last;
      }else{
        this.base = 'USD'
      }
    });
  }
  getCompareTo(){
    this.storage.get('compareTo').then((c)=>{
      if(c){
        this.compareTo = c;
      }else{
        this.compareTo = 'EUR';
      }
    });
  }
  getFromTime(){
    this.storage.get('from').then((f)=>{
      if(f){
        this.from = f;
      }
    });
  }
  getToTime(){
    this.storage.get('to').then((to)=>{
      if(to){
        this.to = to;
      }else{
        this.to = this.datePipe.transformDate(new Date());
      
      }
    })
  }
  public lineChartData:Array<any> = [
    {data: [65, 59, 80, 81, 56, 55, 40], label: 'Series A'},
    {data: [28, 48, 40, 19, 86, 27, 90], label: 'Series B'}
  ];
  public lineChartLabels:Array<any> = ['January', 'February', 'March', 'April', 'May', 'June', 'July'];
  public lineChartOptions:any = {
    responsive: true
  };
  getValues(jsonObject: RateHistory){
    console.log('in getValues');
    this.lineChartLabels = [];
    this.lineChartData[0].data = [];
    this.lineChartData[0].label = this.base;
    this.lineChartData[1].data = [];
    this.lineChartData[1].label = this.compareTo;
    for(let element in jsonObject.rates){
      this.lineChartLabels.push(element);
      this.lineChartData[0].data.push(1);
      this.lineChartData[1].data.push(jsonObject.rates[element][this.compareTo]);
      
    }

  }
  public lineChartColors:Array<any> = [
    { // grey
      backgroundColor: 'rgba(148,159,177,0.2)',
      borderColor: 'rgba(148,159,177,1)',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
    { // dark grey
      backgroundColor: 'rgba(77,83,96,0.2)',
      borderColor: 'rgba(77,83,96,1)',
      pointBackgroundColor: 'rgba(77,83,96,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(77,83,96,1)'
    }
  ];
  
  
  // events
  public chartClicked(e:any):void {
    console.log(e);
  }
  
  public chartHovered(e:any):void {
    console.log(e);
  }
  refreshExRateFromCache(event) {
    this.cacheService
    .get(this.base)
    .mergeMap((cache: Cache<RateLatest>) => cache.refresh())
    .subscribe((exRate) => {
          this.exRate = JSON.parse(JSON.stringify(exRate));
          this.getValues(this.exRate);
          this.getCurrencyList(this.exRate);
    });    
    setTimeout(() => {
      event.target.complete();
    }, 2000); 
  }
  updateDate(base, event){
    if(this.date1 !== undefined && this.date1 !== null){
      this.from = this.datePipe.transformDate(this.date1);
    }
      
    if(this.date2 !== undefined && this.date2 !== null){
      this.to = this.datePipe.transformDate(this.date2);
    }
      
    this.updateBase(base, event);
  }
  updateBase(base, event){
    this.showDropDown = false;
    this.base = base;
    this.storage.set('lasth', base);
    this.registerInCache(base);
    setTimeout(() => {
      event.target.complete();
    }, 2000);  
  }
  updateTo(compTo:string, event){
    this.showDropDownTo = false;
    this.compareTo = compTo;
    this.storage.set('compareTo', compTo);
    this.getValues(this.exRate);
    setTimeout(() => {
      event.target.complete();
    }, 2000);  
  }
  registerInCache(base:string){
    const dataObservable: Observable<RateHistory> = this.exchangProvider.getHistoryFromBase(base, this.from, this.to);
    this.cacheSubscription = this.cacheService.register('exRateHistory', dataObservable)
    .mergeMap((cache: Cache<RateHistory>) => {
      this.cache = cache;
      this.cache.refresh().subscribe();
      return this.cache.get$;
  }).subscribe((exRate) => {
          this.exRate = JSON.parse(JSON.stringify(exRate));
          this.getValues(this.exRate);
          this.getCurrencyList(this.exRate);
        });
  }
  getCurrencyList(jsonObject: RateHistory){
    this.filteredCurrencies = [];
    this.allCurrenciesRates = [];
    this.from = jsonObject.start_at;
    this.base = jsonObject.base;
    this.to = jsonObject.end_at;
    for(let i = 0; i < this.currencyList.length;i++){  
      this.allCurrenciesRates.push({
          id: this.currencyList[i],
          img: '../../../assets/imgs/flags/'+this.currencyList[i]+'.png',
      });
      
      this.filteredCurrencies = Object.assign([],this.allCurrenciesRates);
      this.currenciesSelect = Object.assign([], this.allCurrenciesRates);
      this.currenciesSelect2 = Object.assign([], this.allCurrenciesRates);
    }
      
  }
  toogleDropDown():void {
    this.showDropDown = !this.showDropDown;
  }
  toogleDropDown2():void {
    this.showDropDownTo = !this.showDropDownTo;
  }
  searchSelect(){
    this.showDropDown = true;
    if(this.searchS !== '' && this.searchS !== null && this.searchS !== undefined){
      this.currenciesSelect = this.allCurrenciesRates.filter((currency) => {
        return currency.id.toLowerCase().indexOf(this.searchS.toLowerCase()) > -1;
      });
    }else{
      this.currenciesSelect = Object.assign([],this.allCurrenciesRates);
    }
  }
  searchSelectCompareTo(){
    this.showDropDown = true;
    if(this.searchS2 !== '' && this.searchS2 !== null && this.searchS2 !== undefined){
      this.currenciesSelect2 = this.allCurrenciesRates.filter((currency) => {
        return currency.id.toLowerCase().indexOf(this.searchS2.toLowerCase()) > -1;
      });
    }else{
      this.currenciesSelect2 = Object.assign([],this.allCurrenciesRates);
    }
  }
}
