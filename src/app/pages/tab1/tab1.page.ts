import { Platform } from '@ionic/angular';
import { Observable, Subscription } from 'rxjs/';
import { Component } from '@angular/core';
import { CacheService, Cache } from 'ionic-cache-observable';
import { ExchangesService, RateLatest } from 'src/app/providers/exchanges.service';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {
  public data$: Observable<RateLatest>;
  private cache: Cache<RateLatest>;
  private cacheSubscription: Subscription;
  exRate: RateLatest;
  base: string= '';
  date: string;
  searcher:string;
  searchS: string;
  filteredCurrencies= [];
  allCurrenciesRates = [];
  currenciesSelect = [];
  values = [];
  flags = [];
  quantity: number = 1;
  showList: boolean = false;
  constructor(
    private platform: Platform,
    private cacheService: CacheService,
    private exchangProvider: ExchangesService,
    private storage: Storage
  ) {
    this.platform.ready().then(()=>{
      this.storage.get('last').then((last)=>{
        if(last){
          this.base = last;
        }else{
          this.base = 'USD'
        }
      });
    });
  }

  ionViewWillEnter(){
    this.registerInCache(this.base);
  }
  
  toogleDropDown():void {
    this.showList = !this.showList;
  }
    
  getCurrencyList(jsonObject: RateLatest){
    this.filteredCurrencies = [];
    this.allCurrenciesRates = [];
    this.date = jsonObject.date;
    this.base = jsonObject.base;
    for (let element in jsonObject.rates) {

      this.allCurrenciesRates.push({
          id: element,
          img: '../../../assets/imgs/flags/'+element+'.png',
          value: jsonObject.rates[element]
      });
      this.flags.push('../../../assets/imgs/flags/'+element+'.png');
      this.values[element] =  jsonObject.rates[element];
      this.filteredCurrencies = Object.assign([],this.allCurrenciesRates);
      this.currenciesSelect = Object.assign([], this.allCurrenciesRates);
    }

  }
  
  refreshExRateFromCache(event) {
    this.cacheService
    .get(this.base)
    .mergeMap((cache: Cache<RateLatest>) => cache.refresh())
    .subscribe((exRate) => {
          this.exRate = JSON.parse(JSON.stringify(exRate));
          this.getCurrencyList(this.exRate);
    });    
    setTimeout(() => {
      event.target.complete();
    }, 2000); 
  }

  updateBase(base,event){
    this.showList = false;
    this.base = base;
    this.storage.set('last', base);
    this.registerInCache(base);
    setTimeout(() => {
      event.target.complete();
    }, 2000);  
  }

  registerInCache(base:string){
    const dataObservable: Observable<RateLatest> = this.exchangProvider.getExchange(base);
    this.cacheSubscription = this.cacheService.register('exRate', dataObservable)
    .mergeMap((cache: Cache<RateLatest>) => {
      this.cache = cache;
      this.cache.refresh().subscribe();
      return this.cache.get$;
  }).subscribe((exRate) => {
          this.exRate = JSON.parse(JSON.stringify(exRate));
          this.getCurrencyList(this.exRate);
        });
  }

  searchSelect(){
    this.showList = true;
    if(this.searchS !== '' && this.searchS !== null && this.searchS !== undefined){
      this.currenciesSelect = this.allCurrenciesRates.filter((currency) => {
        return currency.id.toLowerCase().indexOf(this.searchS.toLowerCase()) > -1;
      });
    }else{
      this.currenciesSelect = Object.assign([],this.allCurrenciesRates);
    }
  }

  search(){
    if(this.searcher !== '' && this.searcher !== null && this.searcher !== undefined){
      this.filteredCurrencies = this.allCurrenciesRates.filter((currency) => {
        return currency.id.toLowerCase().indexOf(this.searcher.toLowerCase()) > -1;
      });
    }else{
      this.filteredCurrencies = Object.assign([],this.allCurrenciesRates);
    }
    this.setValues();
  }

  setValues(){
    for(let i = 0; i< this.filteredCurrencies.length; i++){
      this.filteredCurrencies[i].value = this.values[this.filteredCurrencies[i].id] * this.quantity;
    }
  }
  calculate(){
    if(this.quantity !== null){
      for(let i = 0; i < this.filteredCurrencies.length; i++){
        const field = this.values[this.filteredCurrencies[i].id];
        this.filteredCurrencies[i].value = field * this.quantity;
      }
    
    }
  }
}
