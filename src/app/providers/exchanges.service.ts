import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs/';
import 'rxjs/add/observable/defer';
import 'rxjs/add/operator/delay';

import { Rate } from '../models/currencies/currencies.model';

export interface RateLatest{
  rates: Rate,
  base: string,
  date: string
}
export interface RateHistory{
  rates:any,
  base: string,
  start_at: string,
  end_at: string
}

@Injectable({
  providedIn: 'root'
})

export class ExchangesService {

  constructor(private http: HttpClient) { }

  getExchange(base: string = 'USD'): Observable<RateLatest> {
      const url = 'https://api.exchangeratesapi.io/latest?base='+base;
      return this.http.get<RateLatest>(url);
  }
  getHistoryFromBase(base: string = 'USD', from: string = '2019-01-01', to: string = '2019-10-23') : Observable<RateHistory> {
    const url = 'https://api.exchangeratesapi.io/history?start_at='+from+'&end_at='+to+'&base='+base;
    return this.http.get<RateHistory>(url);
  }

  getCurrencyList(){
    return  [
      'CAD',
      'HKD', 
      'ISK',
      'PHP',
      'DKK',
      'HUF',
      'CZK',
      'AUD',
      'RON',
      'SEK',
      'IDR',
      'INR',
      'BRL',
      'RUB',
      'HRK',
      'JPY',
      'THB',
      'CHF',
      'SGD',
      'PLN',
      'BGN',
      'TRY',
      'CNY',
      'NOK',
      'NZD',
      'ZAR',
      'USD',
      'MXN',
      'ILS',
      'GBP',
      'KRW',
      'MYR',
      ];
  }
  getCurrencyListWithImages(){
    return [
      {
      id: 'CAD',
      img: '../../../assets/imgs/flags/CAD.png',
    },{
      id: 'HKD',
      img: '../../../assets/imgs/flags/HKD.png',
    },{
      id: 'ISK',
      img: '../../../assets/imgs/flags/ISK.png',
    },{
      id: 'PHP',
      img: '../../../assets/imgs/flags/PHP.png',
    },{
      id: 'DKK',
      img: '../../../assets/imgs/flags/DKK.png',
    },{
      id: 'HUF',
      img: '../../../assets/imgs/flags/HUF.png',
    },{
      id: 'CZK',
      img: '../../../assets/imgs/flags/CZK.png',
    },{
      id: 'AUD',
      img: '../../../assets/imgs/flags/AUD.png',
    },{
      id: 'RON',
      img: '../../../assets/imgs/flags/RON.png',
    },{
      id: 'SEK',
      img: '../../../assets/imgs/flags/SEK.png',
    },{
      id: 'IDR',
      img: '../../../assets/imgs/flags/IDR.png',
    },{
      id: 'INR',
      img: '../../../assets/imgs/flags/INR.png',
    },{
      id: 'BRL',
      img: '../../../assets/imgs/flags/BRL.png',
    },{
      id: 'RUB',
      img: '../../../assets/imgs/flags/RUB.png',
    },{
      id: 'HRK',
      img: '../../../assets/imgs/flags/HRK.png',
    },{
      id: 'JPY',
      img: '../../../assets/imgs/flags/JPY.png',
    },{
      id: 'THB',
      img: '../../../assets/imgs/flags/THB.png',
    },{
      id: 'CHF',
      img: '../../../assets/imgs/flags/CHF.png',
    },{
      id: 'SGD',
      img: '../../../assets/imgs/flags/SGD.png',
    },{
      id: 'PLN',
      img: '../../../assets/imgs/flags/PLN.png',
    },{
      id: 'BGN',
      img: '../../../assets/imgs/flags/BGN.png',
    },{
      id: 'TRY',
      img: '../../../assets/imgs/flags/TRY.png',
    },{
      id: 'CNY',
      img: '../../../assets/imgs/flags/CNY.png',
    }, {
      id: 'NOK',
      img: '../../../assets/imgs/flags/NOK.png',
    }, {
      id: 'NZD',
      img: '../../../assets/imgs/flags/NZD.png',
    },{
      id: 'ZAR',
      img: '../../../assets/imgs/flags/ZAR.png',
    },{
      id: 'USD',
      img: '../../../assets/imgs/flags/USD.png',
    }, {
      id: 'MXN',
      img: '../../../assets/imgs/flags/MXN.png',
    }, {
      id: 'ILS',
      img: '../../../assets/imgs/flags/ILS.png',
    }, {
      id: 'GBP',
      img: '../../../assets/imgs/flags/GBP.png',
    },{
      id: 'KRW',
      img: '../../../assets/imgs/flags/KRW.png',
    } , {
      id: 'MYR',
      img: '../../../assets/imgs/flags/MYR.png',
    }];
  }
}
