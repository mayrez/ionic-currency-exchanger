# ionic-currency-exchanger

- Ionic app to check exchange rates and calculate exchanges between currencies and to check history exchages between two currencies.
```bash
npm install
ionic serve
```
- To compile to mobile apps run:
```bash
ng build
ionic capacitor add android
ionic capacitor add ios
ionic capacitor copy
ionic capacitor sync
```

